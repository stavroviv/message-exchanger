# Message exchanger

This application receive messages from Apache ActiveMQ, send they to users via email
and store them to mongo database

Two message queues destinations are processed:

ADMIN_MESSAGE_QUEUE used for user activities events (such as login, registration)  
USER_MESSAGE_QUEUE used for user messages (such as new messages, friend requests)

Change application.properties file per your system environment

**Tools:**

+ [JMS](https://spring.io/guides/gs/messaging-jms/)  
+ [MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)  
