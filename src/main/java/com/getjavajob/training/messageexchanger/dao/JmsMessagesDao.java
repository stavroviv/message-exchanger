package com.getjavajob.training.messageexchanger.dao;

import com.getjavajob.training.messageexchanger.model.JmsMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JmsMessagesDao extends MongoRepository<JmsMessage, String> {
}
