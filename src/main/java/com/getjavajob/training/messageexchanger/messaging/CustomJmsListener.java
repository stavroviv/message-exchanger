package com.getjavajob.training.messageexchanger.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.messageexchanger.dao.JmsMessagesDao;
import com.getjavajob.training.messageexchanger.model.JmsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import static com.getjavajob.training.messageexchanger.messaging.ActiveMqConfig.ADMIN_MESSAGE_QUEUE;
import static com.getjavajob.training.messageexchanger.messaging.ActiveMqConfig.USER_MESSAGE_QUEUE;

@Component
public class CustomJmsListener {
    private static final Logger log = LoggerFactory.getLogger(CustomJmsListener.class);
    @Autowired
    private JmsMessagesDao jmsMessagesDao;
    @Autowired
    public JavaMailSender emailSender;

    @JmsListener(destination = ADMIN_MESSAGE_QUEUE)
    public void receiveAdminMessage(TextMessage textMessage) throws JMSException {
        processMessage(textMessage);
    }

    @JmsListener(destination = USER_MESSAGE_QUEUE)
    public void receiveMessageToUser(TextMessage textMessage) throws JMSException {
        JmsMessage jmsMessage = processMessage(textMessage);
        sendEmail(jmsMessage);
    }

    private JmsMessage processMessage(TextMessage textMessage) throws JMSException {
        String message = textMessage.getText();
        JmsMessage jmsMessage = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            jmsMessage = mapper.readValue(message, JmsMessage.class);
            log.info("Received message: " + jmsMessage);
        } catch (Exception e) {
            log.error("error converting to jms message", e);
        }
        jmsMessagesDao.save(jmsMessage);
        return jmsMessage;
    }

    private void sendEmail(JmsMessage jmsMessage) {
        SimpleMailMessage message = new SimpleMailMessage();
        JavaMailSenderImpl jMailSender = (JavaMailSenderImpl) emailSender;
        message.setFrom(jMailSender.getUsername()); // important for yandex mail
        message.setTo(jmsMessage.getUserTo());
        message.setSubject("Social network message");
        message.setText(jmsMessage.getText());
        try {
            emailSender.send(message);
            log.info("Email success sent");
        } catch (MailException e) {
            log.error("Error in sending email", e);
        }
    }
}